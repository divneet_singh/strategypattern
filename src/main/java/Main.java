import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        ManageConnection ctx= new ManageConnection();

        Scanner sc = new Scanner(System.in);
        System.out.println("Choose 1 for mongo 2 for mysql");
        int input= sc.nextInt();
        if ( input==1){
            ctx.setStrategy(new MongoConnectStrategy());
            ctx.InitiateConnection();
        }
        else if (input ==2){
            ctx.setStrategy(new SqlConnectStrategy());
            ctx.InitiateConnection();

        }
        else {
            System.out.println("Operation not permitted");
        }

    }
}

