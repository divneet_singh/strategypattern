public interface ConnectionStrategy {
    //connect function to be used by the strategy you want to implement

    public void connectToDb();

}
