public class ManageConnection {
// or contextmanager
    ConnectionStrategy strategy ;

    public void setStrategy(ConnectionStrategy strategy) {
        this.strategy = strategy;
    }

    public void InitiateConnection(){
        strategy.connectToDb();
    }
}
